#!/usr/bin/env python

import sys, os, subprocess, platform
from glob import glob
#from pybioshell.core.data.io import Pdb
#from pybioshell.utils import LogManager
#LogManager.INFO()

def execute(message, command_line, return_='status', until_successes=False, terminate_on_failure=True, silent=False):
      print(message);  print(command_line)
      while True:

          p = subprocess.Popen(command_line, bufsize=0, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
          output, errors = p.communicate()

          output = output + errors

          if sys.version_info[0] == 2: output = output.decode('utf-8', errors='replace').encode('utf-8', 'replace') # Python-2
          else: output = output.decode('utf-8', errors='replace')  # Python-3

          exit_code = p.returncode

          if exit_code  or  not silent: print(output)

          if exit_code and until_successes: pass  # Thats right - redability COUNT!
          else: break

          print( "Error while executing {}: {}\n".format(message, output) )
          print("Sleeping 60s... then I will retry...")
          time.sleep(60)

      if return_ == 'tuple': return(exit_code, output)

      if exit_code and terminate_on_failure:
          print("\nEncounter error while executing: " + command_line)
          if return_==True: return True
          else: print("\nEncounter error while executing: " + command_line + '\n' + output); sys.exit(1)

      if return_ == 'output': return output
      else: return False

def find_ligand_contacts(pdb_name,ligand_code,cutoff):
  contacts = {}
  hydrogen_location={}

  pdb = Pdb(pdb_name,"",False)
  structure = pdb.create_structure(0)
  for ic in range(structure.count_chains()) :
    lig_chain = structure[ic]
    for ir in range(lig_chain.count_residues()) :
      ligand = lig_chain[ir]
      code3 = ligand.residue_type().code3

      if code3 != ligand_code : continue # Skip other ligands
      for iic in range(structure.count_chains()) :
        other_chain=structure[iic]
        for r in range(other_chain.count_residues()): # ----Iterate over residues
          res=other_chain[r]
          if res == ligand: continue
          d=res.min_distance(ligand) # ---- If residue is close enough to ligand
          if d <cutoff: 
            for ilig in range(ligand.count_atoms()):
              for ioth in range(res.count_atoms()):
                ligand_atom=ligand[ilig]
                other_atom=res[ioth]
                if ligand_atom.distance_to(other_atom) <= cutoff:
                  ligand_atom_name = ligand_atom.atom_name().strip()
                  other_atom_name = other_atom.atom_name().strip()
                  hydrogen_location[ligand_atom_name]=[ligand_atom.x,ligand_atom.y,ligand_atom.z]
                  if ligand_atom_name in contacts:
                    if res.id() in contacts[ligand_atom_name]:
                      contacts[ligand_atom_name][res.id()].append(other_atom_name)
                    else:
                      contacts[ligand_atom_name][res.id()]=[other_atom_name]
                  else:
                      contacts[ligand_atom_name]={res.id():[other_atom_name]}
                
  return contacts,hydrogen_location
  

if __name__=="__main__":
  print(find_ligand_contacts(sys.argv[1],sys.argv[2],float(sys.argv[3])))

