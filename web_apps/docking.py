#!usr/bin/env python3.7
import os, sys,json

from celery import Celery

from flask import render_template, Flask, flash, request, redirect, send_file, jsonify,send_from_directory,url_for
from werkzeug.utils import secure_filename
import requests
sys.path.append("../../../src.git/bioshell/bin/")

from rosetta_dock import sample_ligand_interface
import time
from utils import execute,find_ligand_contacts


UPLOAD_FOLDER = './uploads'
OUTPUT_FOLDER = ""
STATIC_FOLDER = './static'
ALLOWED_EXTENSIONS = set(['txt', 'pdb', 'results', 'rar','pdf'])


ligand_outputs = []
ligand_code = ""
project_name = ""
project_cfg = "project.cfg"
pdb_list = "pdb.files"

app = Flask(__name__,static_url_path='')

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def load_config():
    """Endpoint to load config from a file"""
    global ligand_outputs,ligand_code,OUTPUT_FOLDER
    if not os.path.exists(OUTPUT_FOLDER):
        return {}
    f=open(OUTPUT_FOLDER+project_cfg,"r")
    all_keys = eval(f.read())
    ligand_code = all_keys["ligand_code"]
    return all_keys

@app.route("/clear",methods=['GET'])
def clear():
    """Endpoint to load config from a file"""
    
    global ligand_outputs,ligand_code,OUTPUT_FOLDER,project_name
    ligand_outputs = []
    ligand_code = ""
    project_name = ""
    OUTPUT_FOLDER = ""
    return jsonify({"ok":"yes"})

@app.route("/list_outputs",methods=['POST'])
def list_outputs():
    """Endpoint to list output files on the server."""
    files = []
    for filename in os.listdir(OUTPUT_FOLDER):
        path = os.path.join(OUTPUT_FOLDER, filename)
        if os.path.isfile(path) and ".pdb" in path:
            files.append(filename)
    return jsonify(files)


@app.route("/get_file",methods=['GET', 'POST'])
def get_file():
    name=request.args.get('name',None)
    name=request.form['name']
    print("nazwa",name)
    return  send_file(OUTPUT_FOLDER + name+".pdb",as_attachment=True)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # flask.request.files()
            filename = secure_filename(file.filename)
            file.save(os.path.join('./uploads', filename))
            return redirect(request.url)
    return render_template('docking.html')

@app.route('/dnd_upload.py', methods=["GET", "POST"])
def download_file():
    return send_file("./dnd_upload.py", as_attachment=True)

@app.route('/get_clicked_atoms', methods=['POST'])
def get_clicked_atoms():
    atoms=request.form["atoms"]
    print("ATOMS:",atoms)
    return jsonify({"atoms":"ok"})

@app.route('/process_pdb', methods=['POST'])
def process_pdb():
    global OUTPUT_FOLDER
    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    if not os.path.exists(OUTPUT_FOLDER):
      os.makedirs(OUTPUT_FOLDER)
    fo = open(OUTPUT_FOLDER+request.form["filename"],"w")
    fo.write(request.form["data"])
    fo.close()
    return jsonify({"file":"saved"})


@app.route('/save_params', methods=['POST'])
def save_params():
    global OUTPUT_FOLDER
    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    if not os.path.exists(OUTPUT_FOLDER):
      os.makedirs(OUTPUT_FOLDER)
    fo = open(OUTPUT_FOLDER+request.form["filename"],"w")
    fo.write(request.form["data"])
    fo.close()
    return jsonify({"file":"saved"})

@app.route('/handle_sdf', methods=['POST'])
def handle_sdf():
    global OUTPUT_FOLDER
    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    if not os.path.exists(OUTPUT_FOLDER):
      os.makedirs(OUTPUT_FOLDER)
    fo = open(OUTPUT_FOLDER+request.form["filename"],"w")
    fo.write(request.form["data"])
    fo.close()
    execute("","python2.7 molfile_to_params.py %s --clobber -n %s"%(OUTPUT_FOLDER+ request.form["filename"],OUTPUT_FOLDER+request.form["filename"].split('.')[0]))
    return jsonify({"file":"saved"})

@app.route('/get_score_table', methods=['POST'])
def get_score_table():
    global ligand_outputs,OUTPUT_FOLDER,pdb_list
    
    files=[]
    table={}
    ligand_outputs=[]
    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    files_for_docking = load_config()
    if not os.path.exists(OUTPUT_FOLDER):
        return jsonify({'table':{},'files_for_docking':files_for_docking})
    for filename in os.listdir(OUTPUT_FOLDER):
        path = os.path.join(OUTPUT_FOLDER, filename)
        if os.path.isfile(path) and ".fasc" in path:
            files.append(filename)
    for f in files:
        fo = open(OUTPUT_FOLDER+f,"r")
        lines=fo.readlines()
        for l in lines:
            l=eval(l)
            ligand_outputs.append(l["filename"])
            table[l["filename"].split(".")[0]]=l["total_score"]
    print(table)
    return jsonify({'table':table,'files_for_docking':files_for_docking})

@app.route('/status/<task_id>')
def taskstatus(task_id):
    print(task_id)
    task = dock.AsyncResult(task_id)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@celery.task(bind=True)
def dock(self,pdb_filenames,partners,ligand_params,jobs,first,second,folder):
    global OUTPUT_FOLDER,project_name
    t = time.strftime('%H:%M_%b_%d_%Y')
    job_output = project_name+"_OUT_"+t
    os.chdir(folder)

    try:
        #print("ROSETTA %s"%(ligand_params))
        sample_ligand_interface(pdb_filenames, partners, ligand_params,
        1, job_output,first,second)
    except:
        os.chdir("../../")
        return {'current': 100, 'total': 100, 'status': 'Task failed!',
        'result': 42}
    self.update_state(state='PROGRESS',
                      meta={'current': 'a', 'total': 'b',
                            'status': 'in progress'})
    os.chdir("../../")
    return {'current': 100, 'total': 100, 'status': 'Task completed!',
            'result': 42}

def run_toss_ligand_mover(pdb_name,ligand):
    os.chdir(OUTPUT_FOLDER)

    print("WCHODZE %s %s"%(ligand[0].split(".")[0],pdb_name))
    print(os.getcwd())
    execute("","cat %s_0001.pdb >> %s"%(ligand[0].split(".")[0],pdb_name))
    execute("","python3.7 ../../toss_ligand.py %s X"%(pdb_name))
    os.chdir("../../")

@app.route('/longtask', methods=['POST'])
def prepare_docking():
    global OUTPUT_FOLDER,project_name

    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    project_name = request.form["project_name"]
    if not os.path.exists(OUTPUT_FOLDER):
      os.makedirs(OUTPUT_FOLDER)

    print(os.getcwd())
    pdb_filename = request.form["pdb_name"]
    partners = request.form["partners"]
    ligand_params = eval(request.form["params"])
    run_toss_ligand_mover(pdb_filename,ligand_params)
    jobs = int(request.form["jobs"])
    first = int(request.form["first_cycle"])
    second = int(request.form["second_cycle"])
    f=open(OUTPUT_FOLDER+project_cfg,"w")
    to_write=json.dumps({'pdb':pdb_filename,'partners':partners,'params':ligand_params,'jobs':jobs,'first_cycle':first,'second_cycle':second,'ligand_code':ligand_params[0][:3]})
    f.write(to_write)
    f.close()
    if partners=="AB_X":
        execute("","echo 'HETNAM     HEM B   1  HEM' > header")
    execute("","echo 'HETNAM     %s X   1  %s' >> header"%(ligand_params[0][:3],ligand_params[0][:3]))
    pdb_filenames=[]
    for i in range(10):

        execute("","cat header %sout_%d.pdb >%sout_%d_wh.pdb"%(OUTPUT_FOLDER,i,OUTPUT_FOLDER,i))
        pdb_filenames.append("out_%d_wh.pdb"%i)

    print("ROSETTA ",pdb_filename,partners,ligand_params)
    
    task = dock.apply_async(args=[pdb_filenames,partners,ligand_params,jobs,first,second,OUTPUT_FOLDER])
    return {'Location': url_for('taskstatus',task_id=task.id)}


@app.route("/contact_map", methods=['GET', 'POST'])
def contact_map():
    global ligand_outputs, pdb_list
    cutoff = float(request.form["cutoff"])
    OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
    load_config()
    get_score_table()
    cmap_data={}
    print(ligand_outputs)
    for pdb_name in ligand_outputs:

    #pdb_name = request.form["pdb_name"]
    #ligand_code = request.form["ligand_code"]
        try:
        #print(ligand_outputs)
            contacts,hydrogens=find_ligand_contacts(OUTPUT_FOLDER+pdb_name,ligand_code,cutoff)
            pdb_name = pdb_name.split(".")[0]
            cmap_data[pdb_name]={'contacts':contacts,'hydrogens':hydrogens}
        except:
            pdb_name = pdb_name.split(".")[0]
            cmap_data[pdb_name]={'contacts':[],'hydrogens':[]}
    return jsonify(cmap_data)


@app.route("/upload_pdbid", methods=['GET', 'POST'])
def upload_pdbid():
    #text = request.form['text']
    link = requests.get("https://files.rcsb.org/view/2gb1.pdb")
    filename = "rcsb.pdb"
    with open(filename,"w") as f:
        f.write(link.text)
        # f.close()
    return jsonify(success=True)
    

@app.route('/', methods=['GET', 'POST'])
def upload():

    if request.method == 'POST':
        f = request.files.get('file')
        f.save(os.path.join('./uploads', f.filename))

    return render_template('docking.html')


if __name__ == '__main__':
    app.run(debug=True)
