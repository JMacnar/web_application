import sys

import pyrosetta
import pyrosetta.rosetta as rosetta
from pyrosetta import (
    init, Pose, pose_from_file, MoveMap, get_fa_scorefxn, create_score_function, ScoreFunction,
    SwitchResidueTypeSetMover, MonteCarlo, PyMOLMover, PyJobDistributor
)
from pyrosetta.rosetta import core, protocols

from pyrosetta.rosetta.protocols.loops.loop_mover.perturb import *
from pyrosetta.rosetta.protocols.loops.loop_mover.refine import *
from pyrosetta.rosetta.protocols.loops import Loop, Loops
from pyrosetta.rosetta.core.kinematics import FoldTree

init(set_logging_handler=False)

def model_loop_kic(pdb_filename, job_output, loop_begin, loop_end, loop_cutpoint = '', jobs = 1):

    if loop_cutpoint == '':
        loop_cutpoint = int((loop_begin + loop_end) // 2)
    else:
        loop_cutpoint = int(loop_cutpoint)

    p = Pose()
    pose_from_file(p , pdb_filename)
    starting_p = Pose()
    starting_p.assign(p)


    my_loop = protocols.loops.Loop(loop_begin, loop_end, loop_cutpoint)
    protocols.loops.set_single_loop_fold_tree(p, my_loop)
    protocols.loops.add_single_cutpoint_variant(p, my_loop)
    
    scorefxn_low = create_score_function('cen_std')
    scorefxn_low.set_weight(core.scoring.chainbreak, 1)
    scorefxn_high = get_fa_scorefxn()



    sw = SwitchResidueTypeSetMover("centroid")
        
    sw.apply(p)
    starting_p_centroid = Pose()
    starting_p_centroid.assign(p)
    scorefxn_high(starting_p)

    loops = Loops()
    loops.add_loop(my_loop)

    jd = PyJobDistributor(job_output, jobs, scorefxn_high)
    jd.native_pose = starting_p
    counter = 0
    while not jd.job_complete:
        p.assign(starting_p_centroid)
        counter += 1
        p.pdb_info().name(job_output + '_' + str(counter) + '_cen')

        kic_perturb = LoopMover_Perturb_KIC(loops)
        kic_perturb.apply(p)
        kic_perturb.set_max_kic_build_attempts(1)
        #p.dump_pdb(job_output + "_out_cen.pdb")
        sw = SwitchResidueTypeSetMover("fa_standard")
        sw.apply(p)
        p.pdb_info().name(job_output + '_' + str( counter ) + '_fa')
        kic_refine = LoopMover_Refine_KIC(loops)
        kic_refine.apply(p)
        #p.dump_pdb(job_output + "_out_fa.pdb")
        lrms = protocols.loops.loop_rmsd(p, starting_p, loops, True)
        jd.additional_decoy_info = ' Lrmsd: ' + str(lrms)
        jd.output_decoy(p)


if __name__ == "__main__":
    
    pdb_filename = sys.argv[1]
    loop_begin = int(sys.argv[2])
    loop_end = int(sys.argv[3])

    model_loop_kic(pdb_filename, "blabla",
    loop_begin, loop_end, '', 1)
# ft = FoldTree()
# #ft.add_edge(1, 13, -1)
# #ft.add_edge(13, 19, -1)
# #ft.add_edge(13, 26, 1)
# #ft.add_edge(26, 20, -1)
# #ft.add_edge(26, 129, -1)
# 
# ft.add_edge(1, 101, -1)
# ft.add_edge(101, 104, -1)
# ft.add_edge(101, 108, 1)
# ft.add_edge(108, 105, -1)
# ft.add_edge(108, 129, -1)
# print(ft)
# 
# pdb_filename = sys.argv[1]
# pose = Pose()
# pose_from_file(pose , pdb_filename)
# pose.fold_tree(ft)
# 
# sw = SwitchResidueTypeSetMover("centroid")
# sw.apply(pose)
# loop = Loop(101, 108, 104)
# loops = Loops()
# loops.add_loop(loop)
# 
# kic_perturb = LoopMover_Perturb_KIC(loops)
# kic_perturb.apply(pose)
# kic_perturb.set_max_kic_build_attempts(1)
# pose.dump_pdb("out_cen.pdb")
# 
# sw = SwitchResidueTypeSetMover("fa_standard")
# sw.apply(pose)
# kic_refine = LoopMover_Refine_KIC(loops)
# kic_refine.apply(pose)
# pose.dump_pdb("out_fa.pdb")
# 
