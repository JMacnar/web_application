def succ_get_data(evt):
  global glviewer

  m = glviewer.addModel( evt.text, "pdb" )
  print(m.selectedAtoms())

  atoms = m.selectedAtoms();
  for a in atoms:
            a.clickable = True
            a.callback = atomcallback
                          
  glviewer.setStyle({}, {'stick': {'color':'spectrum'}})
  glviewer.render()
  glviewer.zoomTo()                                                                  
  glviewer.zoom(1.2, 1000)
      
def get_protein(evt): 
    options = document['select_pdb']
    for i in options:
        if i.selected==True:
            name = i.value
    print(name)
    #name="2gb1"
    glviewer.clear();
    req = ajax.ajax()
    content = "application/x-www-form-urlencoded;charset=UTF-8"
    
    req.open('POST', "{{ url_for('get_file') }}", True)
    req.set_header('content-type', content)
    req.bind('complete', succ_get_data)
    req.send({'name' :name})


def add_ligand(evt): 
  v = glviewer
  m = v.addModel(document['text'].innerHTML, "sdf" )              
  m.setStyle({}, {'stick':{}})
  v.zoomTo()                                     
  v.render()                                      


def add_label(evt):
    atoms = glviewer.getModel().selectedAtoms({'resi' : document["labels"].value,'atom': "CA"})
    for atom in atoms: 
        #atom = atoms[a];

        l = glviewer.addLabel(str(atom.resn) + " " + str(atom.rescode), {'inFront' : True, 'fontSize' : 12,'position' : { 'x' : atom.x, 'y' : atom.y,'z' : atom.z }})
        atom.label = l

def on_download(evt):
  glviewer.setBackgroundColor(0xffffffff);
  glviewer.setStyle({}, {'cartoon':{'color':"spectrum"}});
  glviewer.animate({'loop':"backward"});
  glviewer.render();  

def animate(evt):     
      dmol.download("pdb:1MO8",glviewer,{'multimodel':True, 'frames':True},on_download)

def set_SS_color(atom):
    if(atom.ss == 'h'): return "magenta"
    if(atom.ss == 's'): return "orange"
    return "white"

def color_SS(evt): 
    m = glviewer.getModel();
    m.setColorByFunction({}, set_SS_color)
    glviewer.render();

def zoom_ligand(evt): 
    ligands = document['select_ligand']
    for i in ligands:
        if i.selected==True:
            lig = i.value.split(' ')
    #lig = $('#select_ligand option:selected').val().split(' ');
    glviewer.zoomTo({'resn':lig[0],'chain':lig[1]},1000);

def after_download(m):
    m.setStyle({'cartoon':{'colorscheme':{'prop':'ss','map':dmol.ssColors.Jmol}}});
    for i in glviewer.pdbData():
      print(i)
    #print(glviewer.pdbData())
    glviewer.zoomTo();
    glviewer.render();

def download(evt): 
    glviewer.clear();
    glviewer.setBackgroundColor(0xffffffff);
    dmol.download('pdb:'+document['pdbid'].value,glviewer,{'onemol': True,'multimodel': True},after_download) 


        

