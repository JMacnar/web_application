#!usr/bin/env python

from __future__ import print_function

import optparse    # for sorting options

from pyrosetta import init, Pose, Vector1, pose_from_file, create_score_function, PyJobDistributor
from pyrosetta.rosetta import core, protocols

init(extra_options = "-constant_seed")

import os

def sample_ligand_interface(pdb_filenames, partners,
        ligand_params = [''], jobs = 1, job_output = 'ligand_output', first_cycle =4,second_cycle=45):
    """
    Performs ligand-protein docking using Rosetta fullatom docking
        (DockingHighRes) on the ligand-protein complex in  <pdb_filename>
        using the relative chain  <partners>  .
        If the ligand parameters (a .params file, see below) are not defaultly
        loaded into PyRosetta,  <ligand_params>  must supply the list of files
        including the ligand parameters.
        <jobs>  trajectories are performed with output structures named
        <job_output>_(job#).pdb.
    """
    # 1. creates a pose from the desired PDB file
    
    #print("ROSETTA %s %s "%(pdb_filename,ligand_params))
    scorefxn = create_score_function('ligand')
    jd = PyJobDistributor(job_output, len(pdb_filenames), scorefxn)
    while not jd.job_complete:

        counter = 0

        for pdb_filename in pdb_filenames:
            pose = Pose()
            if len(ligand_params) != 0 and ligand_params[0] != '':    # the params list has contents
                ligand = Vector1(ligand_params)
                res_set = pose.conformation().modifiable_residue_type_set_for_conf()
                print(os.getcwd())
                res_set.read_files_for_base_residue_types( ligand )
                pose.conformation().reset_residue_type_set_for_conf( res_set )

            

            pose_from_file(pose, pdb_filename)

            # 2. setup the docking FoldTree
            # using this method, the jump number 1 is automatically set to be the
            #    inter-body jump
            dock_jump = 1
            # the exposed method setup_foldtree takes an input pose and sets its
            #    FoldTree to have jump 1 represent the relation between the two docking
            #    partners, the jump points are the residues closest to the centers of
            #    geometry for each partner with a cutpoint at the end of the chain,
            # the second argument is a string specifying the relative chain orientation
            #    such as "A_B" of "LH_A", ONLY TWO BODY DOCKING is supported and the
            #    partners MUST have different chain IDs and be in the same pose (the
            #    same PDB), additional chains can be grouped with one of the partners,
            #    the "_" character specifies which bodies are separated
            # the third argument...is currently unsupported but must be set (it is
            #    supposed to specify which jumps are movable, to support multibody
            #    docking...but Rosetta doesn't currently)
            # the FoldTrees setup by this method are for TWO BODY docking ONLY!
            protocols.docking.setup_foldtree(pose, partners, Vector1([dock_jump]))

            # 3. create a copy of the pose for testing
            test_pose = Pose()
            test_pose.assign(pose)

            # 4. create ScoreFunctions for centroid and fullatom docking
            

            #### global docking, a problem solved by the Rosetta DockingProtocol,
            ####    requires interface detection and refinement
            #### as with other protocols, these tasks are split into centroid (interface
            ####    detection) and high-resolution (interface refinement) methods
            #### without a centroid representation, low-resolution ligand-protein
            ####    prediction is not possible and as such, only the high-resolution
            ####    ligand-protein interface refinement is available
            #### WARNING: if you add a perturbation or randomization step, the
            ####    high-resolution stages may fail (see Changing Ligand Docking
            ####    Sampling below)
            #### a perturbation step CAN make this a global docking algorithm however
            ####    the rigid-body sampling preceding refinement requires extensive
            ####    sampling to produce accurate results and this algorithm spends most
            ####    of its effort in refinement (which may be useless for the predicted
            ####    interface)

            # 5. setup the high resolution (fullatom) docking protocol (DockMCMProtocol)
            # ...as should be obvious by now, Rosetta applications have no central
            #    standardization, the DockingProtocol object can be created and
            #    applied to perform Rosetta docking, many of its options and settings
            #    can be set using the DockingProtocol setter methods
            # there is currently no centroid representation of an arbitrary ligand in
            #    the chemical database, although you can check to see if it is already
            #    present or make your own (see "Obtaining Params Files" below), and
            #    without a centroid representation, the low-resolution docking stages
            #    are not useful for ligand docking
            docking = protocols.docking.DockMCMProtocol()
            docking.set_scorefxn(scorefxn)
            print("befor_jd")
            docking.set_second_cycle(second_cycle) #default 45
            docking.set_first_cycle(first_cycle) #default 4

            # 6. setup the PyJobDistributor
            print(scorefxn)
            print(jobs)
            print(job_output)
            

            # 7. setup a PyMOL_Observer (optional)
            # the PyMOL_Observer object owns a PyMOLMover and monitors pose objects for
            #    structural changes, when changes are detected the new structure is
            #    sent to PyMOL
            # fortunately, this allows investigation of full protocols since
            #    intermediate changes are displayed, it also eliminates the need to
            #    manually apply the PyMOLMover during a custom protocol
            # unfortunately, this can make the output difficult to interpret (since you
            #    aren't explicitly telling it when to export) and can significantly slow
            #    down protocols since many structures are output (PyMOL can also slow
            #    down if too many structures are provided and a fast machine may
            #    generate structures too quickly for PyMOL to read, the
            #    "Buffer clean up" message
            # uncomment the line below to use PyMOL_Observer
        ##    AddPyMOLObserver(test_pose, True)
            print("after_jd")
            # 8. perform protein-protein docking
                # for pretty output to PyMOL
            counter += 1

            
                # a. set necessary variables for this trajectory
                # -reset the test pose to original (centroid) structure
            test_pose.assign(pose)
            # -change the pose name, for pretty output to PyMOL
            
            test_pose.pdb_info().name(job_output + '_' + str(counter))

            # b. perform docking
            docking.apply(test_pose)

            # c. output the decoy structure:
            # to PyMOL
            test_pose.pdb_info().name(job_output + '_' + str(counter) + '_fa')
            # to a PDB file
            jd.output_decoy(test_pose)



