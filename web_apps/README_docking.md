To run Docking App follow this steps:

1. Activate virtualenv and install 'celery' and 'redis'.

2. Open a second terminal and execute run-redis.sh script.

3. Open a third terminal and start a celery worker:

venv/bin/celery worker -A docking.celery --loglevel=info

4. Go back to your previous terminal and run flask app:

python3.7 docking.py 
