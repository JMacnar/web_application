#!usr/bin/env python3.7
import os, sys, json

from celery import Celery

from flask import render_template, Flask, flash, request, redirect, send_file, jsonify, send_from_directory, url_for
from werkzeug.utils import secure_filename
import requests
# sys.path.append("../../../src.git/bioshell/bin/")

from loop_modelling_kic import model_loop_kic
from loop_modelling_ccd import model_loop_ccd
import time
from utils import execute

UPLOAD_FOLDER = './uploads'
OUTPUT_FOLDER = ""
STATIC_FOLDER = './static'
ALLOWED_EXTENSIONS = {'txt', 'pdb', 'results', 'rar', 'pdf'}

ligand_outputs = []
ligand_code = ""
project_name = ""
project_cfg = "project.cfg"
pdb_list = "pdb.files"

app = Flask(__name__, static_url_path='')

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

selected_mode = ""


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def load_config():
    """Endpoint to load config from a file"""
    # toErase global ligand_outputs,ligand_code,OUTPUT_FOLDER
    global OUTPUT_FOLDER
    if not os.path.exists(OUTPUT_FOLDER):
        return {}
    f = open(OUTPUT_FOLDER + project_cfg, "r")
    all_keys = eval(f.read())
    # toErase ligand_code = all_keys["ligand_code"]
    return all_keys


@app.route("/clear", methods=['GET'])
def clear():
    """Endpoint to load config from a file"""

    global ligand_outputs, ligand_code, OUTPUT_FOLDER, project_name
    ligand_outputs = []
    ligand_code = ""
    project_name = ""
    OUTPUT_FOLDER = ""
    return jsonify({"ok": "yes"})


@app.route("/list_outputs", methods=['POST'])
def list_outputs():
    """Endpoint to list output files on the server."""
    files = []
    for filename in os.listdir(OUTPUT_FOLDER):
        path = os.path.join(OUTPUT_FOLDER, filename)
        if os.path.isfile(path) and ".pdb" in path:
            files.append(filename)
    return jsonify(files)


@app.route("/get_file", methods=['GET', 'POST'])
def get_file():
    name = request.args.get('name', None)
    name = request.form['name']
    print("nazwa", name)
    return send_file(OUTPUT_FOLDER + name + ".pdb", as_attachment=True)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # flask.request.files()
            filename = secure_filename(file.filename)
            file.save(os.path.join('./uploads', filename))
            return redirect(request.url)
    return render_template('looping.html')


@app.route('/dnd_upload.py', methods=["GET", "POST"])
def download_file():
    return send_file("./dnd_upload.py", as_attachment=True)


@app.route('/get_clicked_atoms', methods=['POST'])
def get_clicked_atoms():
    atoms = request.form["atoms"]
    print("ATOMS:", atoms)
    return jsonify({"atoms": "ok"})


@app.route('/process_pdb', methods=['POST'])
def process_pdb():
    global OUTPUT_FOLDER
    OUTPUT_FOLDER = "./" + request.form["username"] + "/" + request.form["project_name"] + "/"
    if not os.path.exists(OUTPUT_FOLDER):
        os.makedirs(OUTPUT_FOLDER)
    fo = open(OUTPUT_FOLDER + request.form["filename"], "w")
    fo.write(request.form["data"])
    fo.close()
    return jsonify({"file": "saved"})


@app.route('/get_score_table', methods=['POST'])
def get_score_table():
    # toErase global ligand_outputs,OUTPUT_FOLDER,pdb_list
    global OUTPUT_FOLDER, pdb_list

    files = []
    table = {}
    # toErase ligand_outputs=[]
    OUTPUT_FOLDER = "./" + request.form["username"] + "/" + request.form["project_name"] + "/"
    file_pdb_name = load_config()
    if not os.path.exists(OUTPUT_FOLDER):
        return jsonify({'table': {}, 'file_pdb_name': file_pdb_name})
    for filename in os.listdir(OUTPUT_FOLDER):
        path = os.path.join(OUTPUT_FOLDER, filename)
        if os.path.isfile(path) and ".fasc" in path:
            files.append(filename)
    for f in files:
        fo = open(OUTPUT_FOLDER + f, "r")
        lines = fo.readlines()
        for l in lines:
            l = eval(l)
            # toErase ligand_outputs.append(l["filename"])
            table[l["filename"].split(".")[0]] = l["total_score"]
    print(table)
    return jsonify({'table': table, 'file_pdb_name': file_pdb_name})


@app.route('/status/<task_id>/<selected_id>')
def taskstatus(task_id, selected_id):
    print("selected_id:")
    print(selected_id)
    if selected_id == 0:
        return taskstatus_kic(task_id)
    else:
        return taskstatus_ccd(task_id)


def taskstatus_kic(task_id):
    print(task_id)
    task = kic_model_loop.AsyncResult(task_id)
    response = check_status(task)
    return jsonify(response)


def taskstatus_ccd(task_id):
    print(task_id)
    task = ccd_model_loop.AsyncResult(task_id)
    response = check_status(task)
    return jsonify(response)


def check_status(task):
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'status': str(task.info),  # this is the exception raised
        }
    return response


@celery.task(bind=True, name='kic_model_loop.task')
def kic_model_loop(self, pdb_filename, folder, filename_begin, loop_begin, loop_end, loop_cut, jobs_number):
    job_output = prepare_output(filename_begin, folder)

    try:
        # print("ROSETTA %s"%(ligand_params))
        # run modelling
        model_loop_kic(pdb_filename, job_output, loop_begin, loop_end, loop_cut, jobs_number)
    except:
        os.chdir("../../")
        return {'current': 100, 'total': 100, 'status': 'Task failed!',
                'result': 42}
    self.update_state(state='PROGRESS',
                      meta={'current': 'a', 'total': 'b',
                            'status': 'in progress'})
    os.chdir("../../")
    return {'current': 100, 'total': 100, 'status': 'Task completed!',
            'result': 42}


@celery.task(bind=True, name='ccd_model_loop.task')
def ccd_model_loop(self, pdb_filename, folder, filename_begin, loop_begin, loop_end, loop_cut,
                   outer_cycles_low, inner_cycles_low, init_temp_low, final_temp_low,
                   outer_cycles_high, inner_cycles_high, init_temp_high, final_temp_high, jobs_number):
    # prepare name for output file
    job_output = prepare_output(filename_begin, folder)
    try:
        # print("ROSETTA %s"%(ligand_params))
        # run modelling
        model_loop_ccd(pdb_filename, job_output, loop_begin, loop_end, loop_cut,
                       outer_cycles_low, inner_cycles_low, init_temp_low, final_temp_low,
                       outer_cycles_high, inner_cycles_high, init_temp_high, final_temp_high, jobs_number)
    except:
        os.chdir("../../")
        return {'current': 100, 'total': 100, 'status': 'Task failed!',
                'result': 42}
    self.update_state(state='PROGRESS',
                      meta={'current': 'a', 'total': 'b',
                            'status': 'in progress'})
    os.chdir("../../")
    return {'current': 100, 'total': 100, 'status': 'Task completed!',
            'result': 42}


def prepare_output(filename_begin, folder):
    # prepare name for output file
    t = time.strftime('%H:%M_%b_%d_%Y')
    job_output = filename_begin + "_OUT_" + t
    os.chdir(folder)
    return job_output


@app.route('/longtask', methods=['POST'])
def prepare_modelling():
    if request.form["selected_mode"] == 'kic':
        return prepare_modelling_kic()
    elif request.form["selected_mode"] == 'ccd':
        return prepare_modelling_ccd()


def prepare_modelling_kic():
    global OUTPUT_FOLDER, project_name

    create_project_dir()

    print(os.getcwd())

    # get pdb_filename
    pdb_filename = request.form["pdb_name"]

    # get loop_cut
    if not request.form["loop_cut"]:
        loop_cut = ''
    else:
        loop_cut = int(request.form["loop_cut"])

    # get jobs number
    jobs_number = int(request.form["jobs_number"])

    # get loop_begin
    loop_begin = int(request.form["loop_begin"])

    # get loop_end
    loop_end = int(request.form["loop_end"])

    # testing - print params
    # print (pdb_filename)
    # print (OUTPUT_FOLDER)
    # print (project_name)
    # print (loop_begin)
    # print (loop_end)
    # print (jobs_number)

    # create output file
    f = open(OUTPUT_FOLDER + project_cfg, "w")
    to_write = json.dumps({'pdb': pdb_filename, 'loop_begin': loop_begin, 'loop_end': loop_end, 'loop_cut': loop_cut,
                           'jobs_number': jobs_number})
    f.write(to_write)
    f.close()

    print("ROSETTA ")  # ,pdb_filename,partners,ligand_params)

    # async execute model_loop function with appropriate params
    task = kic_model_loop.apply_async(
        args=[pdb_filename, OUTPUT_FOLDER, project_name, loop_begin, loop_end, loop_cut, jobs_number])

    return {'Location': url_for('taskstatus', task_id=task.id, selected_id=0)}


def prepare_modelling_ccd():
    global OUTPUT_FOLDER, project_name

    create_project_dir()

    print(os.getcwd())

    # get pdb_filename
    pdb_filename = request.form["pdb_name"]

    # get loop_cut
    if not request.form["loop_cut"]:
        loop_cut = ''
    else:
        loop_cut = int(request.form["loop_cut"])

    # get jobs number
    jobs_number = int(request.form["jobs_number"])

    # get loop_begin
    loop_begin = int(request.form["loop_begin"])

    # get loop_end
    loop_end = int(request.form["loop_end"])

    # get outer_cycles_low
    outer_cycles_low = int(request.form["outer_cycles_low"])

    # get inner_cycles_low
    inner_cycles_low = int(request.form["inner_cycles_low"])

    # get init_temp_low
    init_temp_low = float(request.form["init_temp_low"])

    # get final_temp_low
    final_temp_low = float(request.form["final_temp_low"])

    # get outer_cycles_high
    outer_cycles_high = int(request.form["outer_cycles_high"])

    # get inner_cycles_high
    inner_cycles_high = int(request.form["inner_cycles_high"])

    # get init_temp_high
    init_temp_high = float(request.form["init_temp_high"])

    # get final_temp_low
    final_temp_high = float(request.form["final_temp_high"])

    # create output file
    f = open(OUTPUT_FOLDER + project_cfg, "w")
    to_write = json.dumps({'pdb': pdb_filename, 'loop_begin': loop_begin, 'loop_end': loop_end, 'loop_cut': loop_cut,
                           'outer_cycles_low': outer_cycles_low, 'inner_cycles_low': inner_cycles_low,
                           'init_temp_low': init_temp_low, 'final_temp_low': final_temp_low,
                           'outer_cycles_high': outer_cycles_high, 'inner_cycles_high': inner_cycles_high,
                           'init_temp_high': init_temp_high, 'final_temp_high': final_temp_high,
                           'jobs_number': jobs_number})
    f.write(to_write)
    f.close()

    print("ROSETTA ")  # ,pdb_filename,partners,ligand_params)

    # async execute model_loop function with appropriate params
    task = ccd_model_loop.apply_async(
        args=[pdb_filename, OUTPUT_FOLDER, project_name, loop_begin, loop_end, loop_cut, outer_cycles_low,
              inner_cycles_low, init_temp_low, final_temp_low,
              outer_cycles_high, inner_cycles_high, init_temp_high, final_temp_high, jobs_number])

    return {'Location': url_for('taskstatus', task_id=task.id, selected_id=1)}


def create_project_dir():
    global OUTPUT_FOLDER, project_name
    # create directory
    OUTPUT_FOLDER = "./" + request.form["username"] + "/" + request.form["project_name"] + "/"
    project_name = request.form["project_name"]
    if not os.path.exists(OUTPUT_FOLDER):
        os.makedirs(OUTPUT_FOLDER)


# @app.route("/contact_map", methods=['GET', 'POST'])
# def contact_map():
#     global ligand_outputs, pdb_list
#     cutoff = float(request.form["cutoff"])
#     OUTPUT_FOLDER = "./"+request.form["username"]+"/"+request.form["project_name"]+"/"
#     load_config()
#     get_score_table()
#     cmap_data={}
#     print(ligand_outputs)
#     for pdb_name in ligand_outputs:
# 
#     #pdb_name = request.form["pdb_name"]
#     #ligand_code = request.form["ligand_code"]
#         try:
#         #print(ligand_outputs)
#             contacts,hydrogens=find_ligand_contacts(OUTPUT_FOLDER+pdb_name,ligand_code,cutoff)
#             pdb_name = pdb_name.split(".")[0]
#             cmap_data[pdb_name]={'contacts':contacts,'hydrogens':hydrogens}
#         except:
#             pdb_name = pdb_name.split(".")[0]
#             cmap_data[pdb_name]={'contacts':[],'hydrogens':[]}
#     return jsonify(cmap_data)
#

@app.route("/upload_pdbid", methods=['GET', 'POST'])
def upload_pdbid():
    # text = request.form['text']
    link = requests.get("https://files.rcsb.org/view/2gb1.pdb")
    filename = "rcsb.pdb"
    with open(filename, "w") as f:
        f.write(link.text)
        # f.close()
    return jsonify(success=True)


@app.route('/', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        f = request.files.get('file')
        f.save(os.path.join('./uploads', f.filename))

    return render_template('looping.html')


if __name__ == '__main__':
    app.run(debug=True)
