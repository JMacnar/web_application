import os, sys
from flask import render_template, Flask, flash, request, redirect, send_file, jsonify,send_from_directory
from werkzeug.utils import secure_filename
sys.path.append("../../bioshell/bin/")
from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural import evaluate_phi, evaluate_psi

UPLOAD_FOLDER = './uploads'
STATIC_FOLDER = './static'
ALLOWED_EXTENSIONS = set(['txt', 'pdb', 'results', 'rar','pdf'])

app = Flask(__name__,static_url_path='')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/files")
def list_files():
    """Endpoint to list files on the server."""
    files = []
    for filename in os.listdir(UPLOAD_FOLDER):
        path = os.path.join(UPLOAD_FOLDER, filename)
        if os.path.isfile(path):
            files.append(filename)
    return jsonify(files)

@app.route("/ligands",methods=['GET', 'POST'])
def list_ligands():
    """Endpoint to list files on the server."""
    pdb_name = request.form["name"]
    print(pdb_name)
    ligands = []
    structure = Pdb("uploads/"+pdb_name+".pdb","",False).create_structure(0)
    for ic in range(structure.count_chains()) :
        chain = structure[ic]
        for ir in range(chain.terminal_residue_index() + 1,chain.size()) :
            resid = chain[ir]
            code3 = resid.residue_type().code3
            if resid.residue_type().code3 == "HOH" : continue # Skip water molecules, they are so obvious and abundant
            formula = structure.formula(code3)
            hetname = structure.hetname(code3)
            ligands.append([code3, chain.id(), resid.id(), formula.strip(), hetname.strip()])
            #print("%3s %c %4d %s %s" %(code3, chain.id(), resid.id(), formula.strip(), hetname.strip()))
    return jsonify(ligands)

@app.route("/get_file",methods=['GET', 'POST'])
def get_file():
    name=request.args.get('name',None)
<<<<<<< HEAD
=======
    name=request.form['name']
>>>>>>> 4616582361787a7051a162152b3f7540eb10fdbe
    print("nazwa",name)
    return send_file('uploads/'+ name+".pdb")
    
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # flask.request.files()
            filename = secure_filename(file.filename)
            file.save(os.path.join('./uploads', filename))
            return redirect(request.url)
    return render_template('viewer.html')

@app.route('/dnd_upload.py', methods=["GET", "POST"])
def download_file():
    return send_file("./dnd_upload.py", as_attachment=True)

@app.route('/process_pdb', methods=['POST'])
def process_pdb():
    #print(request.form["filename"])
    fo = open("uploads/"+request.form["filename"],"w")
    fo.write(request.form["pdb"])
    fo.close()
    #os.symlink("uploads/"+request.form["filename"],"static/"+request.form["filename"])
    print("data",request.data)
    output = {}
    output[request.form["filename"].split(".")[-2]] = []
    factor = 180.0/3.14159
    structure = Pdb("./uploads/"+request.form["filename"],"",False).create_structure(0)
    for code in structure.chain_codes() :
      chain = structure.get_chain(code)
      n_res = chain.count_aa_residues()
      for i_res in range(1,n_res-1) :
        try :
          r = chain[i_res]
          r_prev = chain[i_res-1]
          r_next = chain[i_res+1]
          phi = evaluate_phi(r_prev,r)
          psi = evaluate_psi(r,r_next)
          output[request.form["filename"].split(".")[-2]].append([r.id(), r.residue_type().code3, r.owner().id(),phi*factor, psi*factor])
          #print("%d %s %c %7.2f %7.2f" % (r.id(), r.residue_type().code3, r.owner().id(),phi*factor, psi*factor))
        except :
          print("can't evaluate Phi/Psi at position",i_res, file=sys.stderr)
    #return render_template('index.html',filename=request.form['filename'])
    #return request.form["pdb"]
    return jsonify(output)
    
<<<<<<< HEAD
@app.route('/calc_chi', methods=['POST'])
def calc_chi():
    fo = open("uploads/"+request.form["filename"],"w")
    fo.write(request.form["pdb"])
    fo.close()
    print("data",request.data)
    output = {}
    output[request.form["filename"].split(".")[-2]] = []
    
@app.route("/upload_pdbid", methods=['GET', 'POST'])
def upload_pdbid():
    text = request.form['text']
    link = requests.get(f"https://files.rcsb.org/view/{text}.pdb")
    filename = f"{text}.pdb"
    with open(filename) as f:
        f.write(link.text)
        # f.close()
    return jsonify(success=True)
=======
>>>>>>> 4616582361787a7051a162152b3f7540eb10fdbe

@app.route('/', methods=['GET', 'POST'])
def upload():

    if request.method == 'POST':
        f = request.files.get('file')
        f.save(os.path.join('./uploads', f.filename))

    return render_template('viewer.html')


if __name__ == '__main__':
    app.run(debug=True)
