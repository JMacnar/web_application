#!usr/bin/env python

from __future__ import print_function

# import optparse    # for option sorting
import sys
from math import pow  # for decrementing kT during simulated annealing

import pyrosetta
import pyrosetta.rosetta as rosetta

from pyrosetta import (
    init, Pose, pose_from_file, MoveMap, get_fa_scorefxn, create_score_function,
    SwitchResidueTypeSetMover, MonteCarlo, PyMOLMover, PyJobDistributor
)
from pyrosetta.rosetta import core, protocols

from rosetta.protocols.loops.loop_mover.refine import LoopMover_Refine_CCD

init(set_logging_handler=False)  # extra_options = "-constant_seed -run:jran 1111125")


# import os; os.chdir('.test.output')


#########
# Methods

def model_loop_ccd(pdb_filename, job_output,
                   loop_begin, loop_end, loop_cutpoint='',
                   outer_cycles_low=2, inner_cycles_low=5,
                   init_temp_low=2.0, final_temp_low=0.8,
                   outer_cycles_high=5, inner_cycles_high=10,
                   init_temp_high=2.2, final_temp_high=0.6,
                   jobs=1):
    """
    Performs simple single loop construction on the input  <pdb_filename>
        with loop from  <loop_begin>  to  <loop_end>  with a
        cutpoint at  <loop_cutpoint>. <jobs>  trajectories are performed,
        each using a low resolution (centroid) simulated annealing with
        <outer_cycles>  rounds and  <inner_cycles>  steps per round decrementing
        "temperature" from  <init_temp>  to  <final_temp>  geometrically.
        Output structures are named  <job_output>_(job#).pdb.

    """

    if loop_cutpoint == '':
        loop_cutpoint = int((loop_begin + loop_end) // 2)
    else:
        loop_cutpoint = int(loop_cutpoint)

    p = Pose()
    pose_from_file(p, pdb_filename)

    starting_p = Pose()
    starting_p.assign(p)

    my_loop = protocols.loops.Loop(loop_begin, loop_end, loop_cutpoint)
    protocols.loops.set_single_loop_fold_tree(p, my_loop)
    protocols.loops.add_single_cutpoint_variant(p, my_loop)

    movemap = MoveMap()
    movemap.set_bb_true_range(loop_begin, loop_end)
    movemap.set_chi(True)  # sets all chi torsions free

    ccd_closure = protocols.loops.loop_closure.ccd.CCDLoopClosureMover(my_loop, movemap)

    scorefxn_low = create_score_function('cen_std')
    scorefxn_low.set_weight(core.scoring.chainbreak, 1)
    scorefxn_high = get_fa_scorefxn()  # create_score_function_ws_patch('standard', 'score12')

    task_pack = core.pack.task.TaskFactory.create_packer_task(starting_p)
    task_pack.restrict_to_repacking()  # prevents design, packing only
    task_pack.or_include_current(True)  # considers original sidechains
    pack = protocols.minimization_packing.PackRotamersMover(scorefxn_high, task_pack)

    sample_loops = protocols.loops.Loops()
    sample_loops.add_loop(my_loop)
    loop_refine = LoopMover_Refine_CCD(sample_loops)
    loop_refine.temp_initial(init_temp_high)
    loop_refine.temp_final(final_temp_high)
    loop_refine.outer_cycles(outer_cycles_high)
    loop_refine.max_inner_cycles(inner_cycles_high)

    to_centroid = SwitchResidueTypeSetMover('centroid')
    to_fullatom = SwitchResidueTypeSetMover('fa_standard')
    recover_sidechains = protocols.simple_moves.ReturnSidechainMover(starting_p)

    to_centroid.apply(p)
    starting_p_centroid = Pose()
    starting_p_centroid.assign(p)

    gamma = pow((final_temp_low / init_temp_low),
                (1.0 / (outer_cycles_low * inner_cycles_low)))

    pymov = PyMOLMover()
    scorefxn_high(starting_p)  # for exporting the scores
    pymov.apply(starting_p)
    pymov.send_energy(starting_p)

    jd = PyJobDistributor(job_output, jobs, scorefxn_high)
    jd.native_pose = starting_p

    counter = 0  # for exporting to PyMOL
    while not jd.job_complete:
        p.assign(starting_p_centroid)
        counter += 1
        p.pdb_info().name(job_output + '_' + str(counter) + '_cen')
        kT = init_temp_low
        mc = MonteCarlo(p, scorefxn_low, kT)

        for i in range(loop_begin, loop_end + 1):
            p.set_phi(i, -180)
            p.set_psi(i, 180)
        pymov.apply(p)
        pymov.apply(p)

        for i in range(1, outer_cycles_low + 1):
            mc.recover_low(p)  # loads mc's lowest scoring pose into p
            for j in range(1, inner_cycles_low + 1):
                kT = kT * gamma
                mc.set_temperature(kT)
                pymov.apply(p)
                ccd_closure.apply(p)
                pymov.apply(p)
                mc.boltzmann(p)

        mc.recover_low(p)  # loads mc's lowest scoring pose into p
        to_fullatom.apply(p)
        recover_sidechains.apply(p)
        pack.apply(p)
        pymov.apply(p)
        p.pdb_info().name(job_output + '_' + str(counter) + '_fa')

        loop_refine.apply(p)

        lrms = protocols.loops.loop_rmsd(p, starting_p, sample_loops, True)
        jd.additional_decoy_info = ' Lrmsd: ' + str(lrms)
        jd.output_decoy(p)


# parser = optparse.OptionParser()
# parser.add_option('--pdb_filename', dest = 'pdb_filename',
#     default = '../test/data/test_in.pdb',    # default example PDB
#     help = 'the PDB file containing the loop to remodel')
# # the loop options
# parser.add_option('--loop_begin', dest = 'loop_begin',
#     default = '15',    # specific to each inquiry, in this case test_in.pdb
#     help = 'the starting residue of the loop region to remodel' )
# parser.add_option('--loop_end', dest = 'loop_end',
#     default = '19',    # specific to each inquiry, in this case test_in.pdb
#     help = 'the last residue of the loop region to remodel')
# parser.add_option('--loop_cutpoint' , dest = 'loop_cutpoint',
#     default = '',    # specific to each inquiry, in this case test_in.pdb
#     help = 'the cutpoint residue for the loop region')
# # the fragment file options
# parser.add_option('--frag_filename', dest = 'frag_filename',
#     default = '../test/data/test3_fragments',    # specific to each PDB (test_in.pdb here)
#     help = 'the file containing fragments corresponding to the PDB')
# parser.add_option('--frag_length', dest = 'frag_length',
#     default = '3',    # must match the frag_filename
#     help = 'the length of fragments contained in the frag_file')
# # low resolution options
# parser.add_option('--outer_cycles_low', dest = 'outer_cycles_low',
#     default = '2',    # defaults to low value for speed
#     help = 'the number of rounds of simulated annealing to perform\
#         for each trajectory, low resolution')
# parser.add_option('--inner_cycles_low', dest = 'inner_cycles_low',
#     default = '5',    # defaults to low value for speed
#     help = 'the number of steps in a single simulated annealing,\
#         low resolution')
# parser.add_option('--init_temp_low',dest ='init_temp_low',
#     default = '2.0',    # commonly used higher "temperature"
#     help = 'the initial \"temperature\" of the simulated annealing,\
#         low resolution')
# parser.add_option('--final_temp_low', dest = 'final_temp_low',
#     default = '0.8',    # commonly used lower "temperature"
#     help = 'the final \"temperature\" of the simulated annealing,\
#         low resolution')
# # high resolution options
# parser.add_option('--outer_cycles_high', dest = 'outer_cycles_high',
#     default = '5',    # defaults to low value for speed
#     help = 'the number of rounds of simulated annealing to perform\
#         for each trajectory, high resolution')
# parser.add_option('--inner_cycles_high', dest = 'inner_cycles_high',
#     default = '10',    # defaults to low value for speed
#     help = 'the number of steps in a single simulated annealing,\
#         high resolution')
# parser.add_option('--init_temp_high',dest ='init_temp_high',
#     default = '2.2',    # commonly used higher "temperature"
#     help = 'the initial \"temperature\" of the simulated annealing,\
#         high resolution')
# parser.add_option('--final_temp_high', dest = 'final_temp_high',
#     default = '0.6',    # commonly used lower "temperature"
#     help = 'the final \"temperature\" of the simulated annealing,\
#         high resolution')
# # the JobDistributor options
# parser.add_option('--jobs', dest='jobs',
#     default = '1',    # default to single trajectory for speed
#     help = 'the number of jobs (trajectories) to perform')
# parser.add_option('--job_output', dest = 'job_output',
#     default = 'loop_output',    # if a specific output name is desired
#     help = 'the name preceding all output, output PDB files and .fasc')
# (options,args) = parser.parse_args()
# 
# # PDB file option
# pdb_filename = options.pdb_filename
# 
# # loop options
# loop_begin = int(options.loop_begin)
# loop_end = int(options.loop_end)
# # default the loop cutpoint to the average of loop_begin and loop_end
# if options.loop_cutpoint:
#     loop_cutpoint = int(options.loop_cutpoint)
# else:
#     loop_cutpoint = (loop_begin + loop_end) // 2
# # fragment  options
# frag_filename = options.frag_filename
# frag_length = int(options.frag_length)
# # low resolution modeling options (simulated annealing)
# outer_cycles_low = int(options.outer_cycles_low)
# inner_cycles_low = int(options.inner_cycles_low)
# init_temp_low = float(options.init_temp_low)
# final_temp_low = float(options.final_temp_low)
# # high resolution modeling options (a different simulated annealing)
# outer_cycles_high = int(options.outer_cycles_high)
# inner_cycles_high = int(options.inner_cycles_high)
# init_temp_high = float(options.init_temp_high)
# final_temp_high = float(options.final_temp_high)
# # JobDistributor options
# jobs = int(options.jobs)
# job_output = options.job_output

# perform the primary method of this script

if __name__ == "__main__":
    pdb_filename = sys.argv[1]
    job_output = "output_CCD.pdb"
    loop_begin = int(sys.argv[2])
    loop_end = int(sys.argv[3])

    model_loop_ccd(pdb_filename, job_output, loop_begin, loop_end)
#        loop_cutpoint,
#        outer_cycles_low, inner_cycles_low,
#        init_temp_low, final_temp_low ,
#        outer_cycles_high, inner_cycles_high,
#        init_temp_high, final_temp_high,
#        jobs)
