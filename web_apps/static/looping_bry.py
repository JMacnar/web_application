import time
import json, sys
# sys.path.append("core/")
from browser import window, document, ajax, html, timer, alert, bind
# from core.Plot import Plot
# from core.HtmlViewport import *
# from core.svg_elements import *
# from core.styles import *
import os, copy
from viewer_lib import *

print("Loading...")
reader = None
username = ""
file_pdb_name = {'params': []}
already_in_list = []
ajax_data = []  # ajax data recieved
score_list = []  # names that are already in the decoys table
decoy_viewer_pdb = {}  # key is a pdb name and value is a pdb text
decoys_to_show = []  # stores names of decoys to show on glviewer

before_last_atom = ""
last_atom = ""

URL = ""
name = ""
contacts = {}
cntcs = {}
hydrogens = {}
glviewer = window.glviewer
dmol = window.dmol


@bind("#loop_begin", "blur")
def blur(ev):
    document["loop_cut"].value = (int(document["loop_begin"].value) + int(document["loop_end"].value)) // 2


@bind("#loop_end", "blur")
def blur(ev):
    document["loop_cut"].value = (int(document["loop_begin"].value) + int(document["loop_end"].value)) // 2


############ FUNCTIONS TO DISPLAY DECOY TABLE #######################

def clear_vars():
    global username, file_pdb_name, already_in_list, ajax_data, score_list, decoys_to_show, decoy_viewer_pdb, decoys_to_show, URL, name, contacts, cntcs, hydrogens
    username = ""
    file_pdb_name = {'params': []}
    already_in_list = []
    ajax_data = []  # ajax data recieved
    score_list = []  # names that are already in the decoys table
    decoy_viewer_pdb = {}  # key is a pdb name and value is a pdb text
    decoys_to_show = []  # stores names of decoys to show on glviewer

    URL = ""
    name = ""
    contacts = {}
    cntcs = {}
    hydrogens = {}
    for el in document.select('tr')[1:-3]:
        el.clear()
    req = ajax.ajax()
    req.open("GET", "/clear", True)

    req.send({})


def add_decoy_to_list(evt):
    name = evt.getResponseHeader("content-disposition").split("=")[-1].strip("'").strip('"').split(".")[-2]
    decoy_viewer_pdb[name] = evt.text


def show_decoy(name):
    global decoy_viewer_pdb

    m = glviewer.addModel(decoy_viewer_pdb[name], "pdb")

    atoms = m.selectedAtoms();
    for a in atoms:
        a.clickable = True
        a.callback = atomcallback

    glviewer.setStyle({'hetflag': False}, {'cartoon': {'color': 'spectrum'}})
    glviewer.setStyle({'hetflag': True}, {'stick': {'color': 'spectrum'}})


def get_protein(evt):
    global decoys_to_show
    glviewer.clear()

    for i in decoys_to_show:
        show_decoy(i)

    glviewer.render()
    glviewer.zoomTo()


def click_decoy(ev):
    global decoys_to_show
    print("BIND")
    if ev.target.text in decoys_to_show:
        ev.target.attrs["class"] = ""
        decoys_to_show.remove(ev.target.text)
    else:
        decoys_to_show.append(ev.target.text)
        ev.target.attrs["class"] = "bg-white"
    get_protein(ev)


def print_table(req):
    global score_list, decoy, file_pdb_name, project_name
    print(req)
    data = json.loads(req.text)
    print("print_table")
    print(data)
    if data['file_pdb_name'] == {}:
        alert("Project does not exists!")
        return 0
    else:
        file_pdb_name = data['file_pdb_name']

    # print("what")
    if "username" in document:
        username = document["username"].value
        print("user", username)

    for name in data['table']:

        if name not in score_list:
            req = ajax.ajax()
            content = "application/x-www-form-urlencoded;charset=UTF-8"
            req.open('POST', '/get_file', True)
            req.set_header('content-type', content)
            req.bind('complete', add_decoy_to_list)
            req.send({'name': name})

            # name
            td1 = html.TD(name)
            td1.bind('click', click_decoy)

            # total_score
            td2 = html.TD(('%7.3f') % data['table'][name])

            # link
            baseUrl = ""  # toDo add appropriate baseURL
            td3 = html.TD(
                '<a href="' + baseUrl + '/' + username + '/' + project_name + '/' + name + '.pdb" target="_blank">LINK</a>')

            # create tr
            tr = html.TR(td1 + td2 + td3)
            tr.attrs["class"] = ""
            tr.attrs["id"] = str(len(score_list))
            score_list.append(name)
            document.select('tbody')[0] <= tr

    # toErase get_cmap()
    document["inputParams"].style.display = "none"
    document["table"].style.display = "block"
    # toErase document["map_pic"].style.display="none"
    # toErase document["cmap"].style.display="none"


def get_score_table(ev):
    global username, project_name
    document["download"].text = "New project"
    req = ajax.ajax()
    req.open('POST', '/get_score_table', True)
    content = "application/x-www-form-urlencoded;charset=UTF-8"
    # content = "application/json;charset=UTF-8"
    req.set_header('content-type', content)
    req.bind('complete', print_table)
    try:
        if document["username"] != "" and document["project_name"] != "":
            username = document["username"].value
            project_name = document["project_name"].value
        else:
            alert("Missing project name or username")
    except:
        print("get_score_table exception")
    if username != "" and project_name != "":
        req.send({'username': username, 'project_name': project_name})
    else:
        alert("Missing project name or username")

    # if "username" in document and "project_name" in document:
    #     req.send({'username':document["username"].value,'project_name':document["project_name"].value})
    # else:
    #     if username!="" and project_name!="":
    #         req.send({'username':username,'project_name':project_name})
    #     else:
    #         alert("Type project name and username")


######### OTHER VIEWER FUNCTIONS ###############
def show_contact(evt):
    global cntcs, ligand_code
    x, y = return_labels(evt)
    print(x.text)  # receptor residue number
    print(y.text)  # ligand atom name

    # ligand_atom = glviewer.selectedAtoms({'hetflag':True})
    # for i in ligand_atom:
    #     print(i.to_dict())
    add_hydrogen()

    ligand_atom = glviewer.selectedAtoms(
        {'hetflag': True, 'resn': file_pdb_name['ligand_code'], 'atom': y.text.strip()})
    print(ligand_atom[0].to_dict())

    print(cntcs)

    rcptr_atoms = cntcs[y.text.strip()][x.text.strip()]
    print(rcptr_atoms)

    receptor_atom = glviewer.selectedAtoms({'resi': x.innerHTML, 'atom': rcptr_atoms[0].strip()})
    print(receptor_atom[0].to_dict())

    glviewer.addCylinder({
        'start': {'x': ligand_atom[0].x, 'y': ligand_atom[0].y, 'z': ligand_atom[0].z},
        'end': {'x': receptor_atom[0].x, 'y': receptor_atom[0].y, 'z': receptor_atom[0].z},
        'radius': 0.1, 'color': 'grey', 'dashed': True})
    glviewer.addStyle({'resi': x.innerHTML}, {'stick': {'color': 'spectrum'}})

    glviewer.zoomTo({'chain': 'X'}, 500);

    glviewer.render()


def show_prot(evt):
    """Displays input PDB file
    """

    m = glviewer.addModel(document["text"].innerHTML, "pdb")

    atoms = m.selectedAtoms();
    for a in atoms:
        a.clickable = True
        a.callback = atomcallback

    glviewer.setStyle({'hetflag': False}, {'cartoon': {'color': 'spectrum'}})
    glviewer.setStyle({'hetflag': True}, {'stick': {'color': 'spectrum'}})
    glviewer.render()
    glviewer.zoomTo()


def check_clicked_atoms(ev):
    global glviewer
    clicked_atoms = []
    model = glviewer.getModel()
    atoms = model.selectedAtoms()
    for i in atoms:
        if 'clicked' in i.to_dict() and i.clicked == True:
            clicked_atoms.append(int(i.serial))
    print("Clicked: ")
    print(clicked_atoms)
    clicked_atoms = json.dumps(clicked_atoms)
    return clicked_atoms


################# SERVER CONNECTION FUNCTIONS #########################

def handle_file_dragged_over(evt):
    evt.stopPropagation()
    evt.preventDefault()
    evt.dataTransfer.dropEffect = 'copy'


def on_complete(evt):
    global ajax_data
    # print("AJAX COMPLETED, returned:",evt.text)
    ajax_data = json.loads(evt.text)
    print(ajax_data)


def to_timeout():
    global URL
    # toErase get_score_table(0)
    update_progress(URL)  # aio.run(update_progress)


def print_progress(request):
    print(request.text)
    data = json.loads(request.text)
    print(data)
    if data['state'] != 'PENDING' and data['state'] != 'PROGRESS':
        if 'result' in data:
            # show result
            document["download"].innerHTML = "New project"
            document["label_done"].innerHTML = "<b>DONE!</b> Enjoy!"

        else:
            # something unexpected happened
            document['download'].text = 'Error: ' + str(data['state'])
    else:
        # rerun in 2 seconds
        timer.set_timeout(to_timeout, 5000)


def update_progress(status_url):
    global URL
    URL = status_url
    # req = await aio.ajax()
    req = ajax.ajax()
    req.bind('complete', print_progress)
    req.open('GET', status_url, True)
    req.send()


def listen(request):
    status_url = json.loads(request.text)['Location']
    print(status_url)
    update_progress(status_url)


def start_modelling(ev):
    if document['selected_mode'].value == 'kic':
        model_loop_bry_kic(ev)
    elif document['selected_mode'].value == 'ccd':
        model_loop_bry_ccd(ev)


def model_loop_bry_kic(ev):
    global glviewer

    # if == "New project" => clear data and change text on button "download" to "Submit"
    if document["download"].text == "New project":
        # for child in document["cmap"].children:
        #     child.style.display="inline"
        # toErase document["map_pic"].style.display=None
        document["table"].style.display = "none"
        document["inputParams"].style.display = "block"
        document["label_done"].innerHTML = ""
        document["label_upload_pdb_file"].text = "1. Drop PDB file with receptor"
        document['extendedParams'].style.display = "none"
        document["selected_mode"].value = "kic"
        choose_kic(ev)
        clear_vars()
        document["download"].text = "Submit"

    else:
        # change text on button "download" to "Modeling..."
        # prepare ajax on /longtask
        req = ajax.ajax()
        req.open('POST', '/longtask', True)
        content = "application/x-www-form-urlencoded;charset=UTF-8"
        # content = "application/json;charset=UTF-8"
        req.set_header('content-type', content)
        req.bind('complete', listen)

        # execute check_clicked_atoms
        clicked_atoms = check_clicked_atoms(ev)

        try:
            # execute ajax on /longtask
            req.send({'selected_mode': document['selected_mode'].value, 'username': document['username'].value,
                      'project_name': document['project_name'].value, 'pdb_name': file_pdb_name['pdb_name'],
                      'atoms': clicked_atoms, 'loop_begin': document['loop_begin'].value,
                      'loop_end': document['loop_end'].value, 'loop_cut': document['loop_cut'].value,
                      'jobs_number': document['jobs_number'].value})
            document["download"].innerHTML = "Modeling..."
        except:
            alert("Missing file or parameter!")


def model_loop_bry_ccd(ev):
    global glviewer

    # if == "New project" => clear data and change text on button "download" to "Submit"
    if document["download"].text == "New project":
        # for child in document["cmap"].children:
        #     child.style.display="inline"
        # toErase document["map_pic"].style.display=None
        document["table"].style.display = "none"
        document["inputParams"].style.display = "block"
        document['extendedParams'].style.display = "block"
        document["label_done"].innerHTML = ""
        document["label_upload_pdb_file"].text = "1. Drop PDB file with receptor"
        document["selected_mode"].value = "ccd"
        choose_ccd(ev)
        clear_vars()
        document["download"].text = "Submit"

    else:
        # change text on button "download" to "Modeling..."
        # prepare ajax on /longtask
        req = ajax.ajax()
        req.open('POST', '/longtask', True)
        content = "application/x-www-form-urlencoded;charset=UTF-8"
        # content = "application/json;charset=UTF-8"
        req.set_header('content-type', content)
        req.bind('complete', listen)

        # execute check_clicked_atoms
        clicked_atoms = check_clicked_atoms(ev)

        try:
            # execute ajax on /longtask
            req.send({'selected_mode': document['selected_mode'].value, 'username': document['username'].value,
                      'project_name': document['project_name'].value,
                      'pdb_name': file_pdb_name['pdb_name'], 'atoms': clicked_atoms,
                      'loop_begin': document['loop_begin'].value, 'loop_end': document['loop_end'].value,
                      'loop_cut': document['loop_cut'].value, 'jobs_number': document['jobs_number'].value,
                      'outer_cycles_low': document['outer_cycles_low'].value,
                      'inner_cycles_low': document['inner_cycles_low'].value,
                      'init_temp_low': document['init_temp_low'].value,
                      'final_temp_low': document['final_temp_low'].value,
                      'outer_cycles_high': document['outer_cycles_high'].value,
                      'inner_cycles_high': document['inner_cycles_high'].value,
                      'init_temp_high': document['init_temp_high'].value,
                      'final_temp_high': document['final_temp_high'].value})
            document["download"].innerHTML = "Modeling..."
        except:
            alert("Missing file or parameter!")


def send_ajax(data):
    req = ajax.ajax()
    ending = name.split('.')[-1]
    if ending == 'pdb':
        req.open('POST', "/process_pdb", True)
        file_pdb_name['pdb_name'] = name
        document["label_upload_pdb_file"].text = name + ' file loaded!'

    content = "application/x-www-form-urlencoded;charset=UTF-8"
    # content = "application/json;charset=UTF-8"
    req.set_header('content-type', content)
    req.bind('complete', on_complete)
    req.send({'data': data, 'filename': name, 'username': document['username'].value,
              'project_name': document['project_name'].value})


def load_file(evt):
    text = reader.result
    print(text[0:200])
    send_ajax(text)
    if name.split(".")[-1] == 'pdb':
        document["text"].innerHTML = text
        event = window.MouseEvent.new("click")
        document["show_native"].dispatchEvent(event)


def handle_file_dropped(evt):
    global name

    evt.stopPropagation()
    evt.preventDefault()

    files = evt.dataTransfer.files  # FileList object.
    global reader
    reader = window.FileReader.new()
    name = files[0].name
    reader.onload = load_file
    ending = files[0].name.split('.')[-1]
    if ending != 'pdb' and ending != "params" and ending != "sdf":
        alert("Unknown input format .%s" % (ending))

    reader.readAsText(files[0])


def return_labels(ev):
    id = ev.target.id
    x = float(document[id]["x"]) + float(document[id].width) / 2
    y = float(document[id]["y"]) + float(document[id].height) / 2
    x_texts = document["XaxisTicsLabB"].get(selector='text')
    y_texts = document["YaxisTicsLabL"].get(selector='text')
    for i in range(len(x_texts)):
        if x_texts[i]["x"] == str(x):
            x = x_texts[i]
    for i in range(len(y_texts)):
        if y_texts[i]["y"] == str(y):
            return x, y_texts[i]


def bold_label(ev):
    ev.target["fill"] = "red"
    x, y = return_labels(ev)
    x["fill"] = "red"
    x["font-weight"] = "bold"
    y["fill"] = "red"
    y["font-weight"] = "bold"


def normal_label(ev):
    ev.target["fill"] = "SteelBlue"
    x, y = return_labels(ev)
    x["fill"] = "black"
    x["font-weight"] = "normal"
    y["fill"] = "black"
    y["font-weight"] = "normal"
    # cokolwiek


# def draw_cmap(ligand_data, receptor_for_plot, ligand_fracs, ligand_labels, receptor_fracs, receptor_labels):
#     document["cmap"].style.display = None
#     document["map_pic"].style.display = "block"
#
#     drawing = HtmlViewport(document["map_pic"], 0, 0, 450, 450)
#
#     pl = Plot(drawing, 20, 380, 30, 370, -1, len(receptor_labels) - 2, min(ligand_data) - 1, max(ligand_data) + 1,
#               axes_definition="UBLR")
#
#     for ax_key in pl.axes:
#         pl.axes[ax_key].are_tics_inside = False
#
#     pl.axes["L"].tics_at_fraction(ligand_fracs, ligand_labels)
#     pl.axes["U"].tics_at_fraction(receptor_fracs, receptor_labels)
#     pl.axes["B"].tics_at_fraction(receptor_fracs, receptor_labels)
#     pl.axes["R"].tics_at_fraction(ligand_fracs, ligand_labels)
#
#     # pl.axes["U"].label="RECEPTOR"
#     # pl.axes["L"].label="LIGAND"
#
#     mrk_s = int(350 / len(ligand_labels))
#
#     pl.scatter(receptor_for_plot, ligand_data, markersize=mrk_s, markerstyle="s", colors="SteelBlue")
#     for loc, ax in pl.axes.items():
#         ax.width, ax.stroke = 2, color_by_name("SteelBlue").create_darker(0.3)
#     pl.draw_axes()
#     drawing.finalize()
#
#     for i in document.select("rect"):
#         i.bind("mouseover", bold_label)
#         i.bind("mouseout", normal_label)
#         i.bind("click", show_contact)
#
#
# def prepare_data_cmap(to_display):
#     global contacts, cntcs
#     print("CONTACTS")
#     print(contacts)
#
#     if len(to_display) != 0:
#         cntcs = contacts[to_display[0]]
#     else:
#         cntcs = contacts[0]
#
#     # print(contacts)
#     ligand_data = []
#     ligand_labels = []
#     receptor_data = []
#     cnt = 0
#     for ligand_atom in cntcs:
#         for res in cntcs[ligand_atom]:
#             ligand_data.append(cnt)
#             receptor_data.append(int(res))
#         ligand_labels.append(ligand_atom)
#         cnt += 1
#
#     sorted_receptor = sorted(receptor_data)  # sort list
#     sorted_receptor = list(dict.fromkeys(sorted_receptor))  # uniq list
#     result = map(lambda x: str(x), sorted_receptor)  # make a string from numbers
#     receptor_labels = [" "] + list(result)
#     receptor_fracs = []
#
#     for i in range(len(receptor_labels)):
#         receptor_fracs.append(i / (len(receptor_labels)))
#     receptor_fracs.append((i + 1) / len(receptor_labels))
#     receptor_labels.append(" ")
#
#     receptor_for_plot = []
#     cnt = 0
#     for i in range(len(receptor_data)):
#         receptor_for_plot.append(sorted_receptor.index(receptor_data[i]))
#
#     ligand_labels = [" "] + ligand_labels
#     ligand_fracs = []
#
#     for i in range(len(ligand_labels)):
#         ligand_fracs.append(i / (len(ligand_labels)))
#     ligand_fracs.append((i + 1) / len(ligand_labels))
#     ligand_labels.append(" ")
#
#     if len(cntcs) != 0:
#         draw_cmap(ligand_data, receptor_for_plot, ligand_fracs, ligand_labels, receptor_fracs, receptor_labels)
#     else:
#         alert("There is no contacts for these decoy")
#
#
# def add_cmap_data(ev):
#     global contacts, hydrogens
#     data = json.loads(ev.text)
#     for name in data:
#         contacts[name] = data[name]['contacts']
#         hydrogens[name] = data[name]['hydrogens']
#
#
# def get_cmap():
#     global username, project_name
#     req = ajax.ajax()
#     req.open('POST', '/contact_map', True)
#     content = "application/x-www-form-urlencoded;charset=UTF-8"
#     req.set_header('content-type', content)
#     req.bind('complete', add_cmap_data)
#     # params=file_pdb_name['params']
#     if "username" in document and 'project_name' in document:
#         username = document["username"].value
#         project_name = document["project_name"].value
#     # req.send({'pdb_name':name, 'ligand_code':params[0].split(".")[0] ,'cutoff' : document['cutoff'].value})
#     req.send({'username': username, 'project_name': project_name, 'cutoff': document['cutoff'].value})
#
#
# def show_cmap(ev):
#     document["table"].style.display = "none"
#     to_display = []
#     options = document.select('td')
#     for i in options:
#         if "class" in i.attrs and i["class"] == "bg-white":
#             to_display.append(i.text)
#     prepare_data_cmap(to_display)


def atomcallback(atom, viewer, b, a):
    global before_last_atom, last_atom

    res = viewer.selectedAtoms({'resi': atom.resi})
    for at in res:
        if 'clickLabel' not in at.to_dict() or at.clickLabel == None:
            glviewer.addStyle({'resi': atom.resi}, {'stick': {'color': 'spectrum'}});
            glviewer.render();

            for a in res:
                a.clicked = True
                if a.atom == "CA":
                    a.clickLabel = viewer.addLabel(str(atom.resn) + str(atom.rescode),
                                                   {'fontSize': 14,
                                                    'position': {'x': a.x, 'y': a.y, 'z': a.z}});
            at.clickLabel = True
            glviewer.render();

        else:
            at.clickLabel = None
            for a in res:
                if a.atom == "CA":
                    viewer.removeLabel(a.clickLabel);
                   # viewer.removeLabel(at.clickLabel);
                   # glviewer.removeLabel(a.clickLabel);
                   # glviewer.removeLabel(at.clickLabel);
                   # viewer.removeAllLabels();
                    glviewer.setStyle({'resi': atom.resi}, {'cartoon': {'color': 'spectrum'}});
                    viewer.render();
                    a.clickLabel = None
                    print(a.clickLabel)
                a.clicked = False
                print(at.clickLabel)
            glviewer.render()
         
    print("Clicked atom ", str(atom.resn) + str(atom.rescode), at.clickLabel, a.clickLabel)

    if last_atom != int(atom.rescode):
        aa_for_loop_modelling(atom)


def aa_for_loop_modelling(atom):
    global before_last_atom, last_atom
    before_last_atom = last_atom
    last_atom = int(atom.rescode)
    print("Before last:", before_last_atom)
    print("Last:", last_atom)
    if before_last_atom != "" and last_atom != "":
        if last_atom > before_last_atom:
            document['loop_begin'].value = before_last_atom
            document['loop_end'].value = last_atom

        else:
            document['loop_begin'].value = last_atom
            document['loop_end'].value = before_last_atom

        document['loop_cut'].value = (before_last_atom + last_atom) // 2


def add_hydrogen():
    global name
    atom_name = 'H2'
    coordinates = hydrogens[decoys_to_show[0]]
    m = glviewer.addModel()

    # m.addAtoms([{'elem': 'C', 'x': 0, 'y': 0, 'z': 0, 'bonds': [1,2], 'bondOrder': [1,2]}, {'elem': 'O', 'x': -1.5, 'y': 0, 'z': 0, 'bonds': [0]},{'elem': 'O', 'x': 1.5, 'y': 0, 'z': 0, 'bonds': [0], 'bondOrder': [2]}])
    for a in coordinates:
        glviewer.getModel(0).addAtoms(
            [{'hetflag': True, 'elem': a, 'x': coordinates[a][0], 'y': coordinates[a][1], 'z': coordinates[a][2]}])
        at = glviewer.selectedAtoms({'hetflag': True, 'elem': a})
        glviewer.setStyle({'hetflag': True, 'elem': a}, {'stick': {'color': 'black'}})

        print(at[0].to_dict())
    # glviewer.zoomTo(a[0]);
    glviewer.render();


def choose_kic(ev):
    document['btn_kic'].classList.remove('bg-white')
    document['btn_kic'].classList.add('bg-warning')

    document['btn_ccd'].classList.remove('bg-warning')
    document['btn_ccd'].classList.add('bg-white')

    document['extendedParams'].style.display = "none"
    document['selected_mode'].value = "kic"


def choose_ccd(ev):
    document['btn_ccd'].classList.remove('bg-white')
    document['btn_ccd'].classList.add('bg-warning')

    document['btn_kic'].classList.remove('bg-warning')
    document['btn_kic'].classList.add('bg-white')

    document['extendedParams'].style.display = "block"
    document['selected_mode'].value = "ccd"


######################### Setup the listeners ############################
drop_rec = document['inputParams']
drop_rec.bind('dragover', handle_file_dragged_over)
drop_rec.bind('drop', handle_file_dropped)
document["list_files"].bind('click', get_score_table)
document["show_native"].bind('click', show_prot)
document["download"].bind('click', start_modelling)
# document["cmap_button"].bind('click',show_cmap)
document['btn_kic'].bind('click', choose_kic)
document['btn_ccd'].bind('click', choose_ccd)

for i in document.select(".option"): i.bind('click', toggleTab)

print("Done!")
