import time
import json,sys
sys.path.append("core/")
from browser import window, document,ajax,html,timer,alert,bind

from core.Plot import Plot
from core.HtmlViewport import *
from core.svg_elements import *
from core.styles import *
import os,copy
from viewer_lib import *


print("Loading...")
reader = None
username = ""
files_for_docking={'params':[]}
already_in_list=[] 
ajax_data = [] #ajax data recieved
score_list = [] #names that are already in the decoys table
decoy_viewer_pdb = {} #key is a pdb name and value is a pdb text 
decoys_to_show = [] #stores names of decoys to show on glviewer

URL=""
name=""
contacts={}
cntcs={}
hydrogens={}
glviewer=window.glviewer
dmol=window.dmol



############ FUNCTIONS TO DISPLAY DECOY TABLE #######################

def clear_vars():
    global username,files_for_docking,already_in_list,ajax_data,score_list,decoys_to_show,decoy_viewer_pdb,decoys_to_show,URL,name,contacts,cntcs,hydrogens
    username = ""
    files_for_docking={'params':[]}
    already_in_list=[] 
    ajax_data = [] #ajax data recieved
    score_list = [] #names that are already in the decoys table
    decoy_viewer_pdb = {} #key is a pdb name and value is a pdb text 
    decoys_to_show = [] #stores names of decoys to show on glviewer

    URL=""
    name=""
    contacts={}
    cntcs={}
    hydrogens={}
    for el in document.select('tr')[1:]:
        el.clear()
    req = ajax.ajax()
    req.open("GET","/clear",True)

    req.send({})

def add_decoy_to_list(evt):
    name=evt.getResponseHeader("content-disposition").split("=")[-1].strip("'").strip('"').split(".")[-2]
    decoy_viewer_pdb[name]=evt.text


def show_decoy(name):
  global decoy_viewer_pdb
  
  m = glviewer.addModel( decoy_viewer_pdb[ name], "pdb" )

  atoms = m.selectedAtoms();
  for a in atoms:
           
            a.clickable = True
            a.callback = atomcallback

  glviewer.setStyle({'hetflag':False},{'cartoon': {'color':'spectrum'}})
  glviewer.setStyle({'hetflag':True},{'stick': {'color':'spectrum'}})
  

def get_protein(evt):
    global decoys_to_show
    glviewer.clear()
    
    for i in decoys_to_show:
        show_decoy(i)
        
    glviewer.render()
    glviewer.zoomTo()
    


def click_decoy(ev):
    global decoys_to_show
    print("BIND")
    if ev.target.text in decoys_to_show:
        ev.target.attrs["class"]=""
        decoys_to_show.remove(ev.target.text)
    else:
        decoys_to_show.append(ev.target.text)
        ev.target.attrs["class"]="bg-white"
    get_protein(ev)

def print_table(req):
    global score_list,decoy,files_for_docking
    print(req)
    data=json.loads(req.text)
    print("print_table")
    print(data)
    if data['files_for_docking']=={}:
        alert("Project does not exists!")
        return 0
    else:
        files_for_docking = data['files_for_docking']

    print("what")
    if "username" in document:
      print("tu")
      username = document["username"].value

    for name in data['table']:
        
        if name not in score_list:
            
            req = ajax.ajax()
            content = "application/x-www-form-urlencoded;charset=UTF-8"
            req.open('POST', '/get_file', True)
            req.set_header('content-type', content)
            req.bind('complete', add_decoy_to_list)
            req.send({'name' :name})
            
            tr=html.TR(html.TD(name)+html.TD(('%7.3f')%data['table'][name]))
            tr.attrs["class"]=""
            tr.attrs["id"]=str(len(score_list))
            score_list.append(name)
            document.select('tbody')[0]<=tr
    
            tr.bind('click',click_decoy)
    get_cmap()
    document["table"].style.display="block"
    document["map_pic"].style.display="none"
    document["cmap"].style.display="none"


def get_score_table(ev) :
    global username,project_name
    document["download"].text="New project"
    req = ajax.ajax()
    req.open('POST', '/get_score_table', True)
    content = "application/x-www-form-urlencoded;charset=UTF-8"
    #content = "application/json;charset=UTF-8"
    req.set_header('content-type', content)
    req.bind('complete', print_table)
    try:
        if document["username"]!="" and document["project_name"]!="":
            username = document["username"].value
            project_name = document["project_name"].value
        else:
            alert("Missing project name or username")
    except:
        print("toiaaa")
    if username!="" and project_name!="":
        req.send({'username':username,'project_name':project_name})
    else:
        alert("Missing project name or username")

    # if "username" in document and "project_name" in document:
    #     req.send({'username':document["username"].value,'project_name':document["project_name"].value})
    # else:
    #     if username!="" and project_name!="":
    #         req.send({'username':username,'project_name':project_name})
    #     else:
    #         alert("Type project name and username")



######### OTHER VIEWER FUNCTIONS ###############
def show_contact(evt):
    global cntcs,ligand_code
    x,y = return_labels(evt)
    print(x.text) #receptor residue number
    print(y.text) #ligand atom name

    # ligand_atom = glviewer.selectedAtoms({'hetflag':True})
    # for i in ligand_atom:
    #     print(i.to_dict())
    add_hydrogen()

    ligand_atom = glviewer.selectedAtoms({'hetflag':True,'resn':files_for_docking['ligand_code'],'atom':y.text.strip()})
    print(ligand_atom[0].to_dict())

    print(cntcs)

    rcptr_atoms = cntcs[y.text.strip()][x.text.strip()]
    print(rcptr_atoms)

    receptor_atom = glviewer.selectedAtoms({'resi':x.innerHTML,'atom':rcptr_atoms[0].strip()})
    print(receptor_atom[0].to_dict())

    glviewer.addCylinder({
                      'start': {'x':ligand_atom[0].x, 'y':ligand_atom[0].y, 'z':ligand_atom[0].z},
                      'end': {'x':receptor_atom[0].x, 'y':receptor_atom[0].y, 'z':receptor_atom[0].z},
                      'radius': 0.1,'color':'grey','dashed':True})
    glviewer.addStyle({'resi':x.innerHTML},{'stick':{'color':'spectrum'}})

    glviewer.zoomTo({'chain':'X'},500);

    glviewer.render()

def show_prot(evt):
    """Displays input PDB file
    """
    
    m = glviewer.addModel( document["text"].innerHTML, "pdb" )
  

    atoms = m.selectedAtoms();
    for a in atoms:
        a.clickable = True
        a.callback = atomcallback
                              
    glviewer.setStyle({'hetflag':False},{'cartoon': {'color':'spectrum'}})
    glviewer.setStyle({'hetflag':True},{'stick': {'color':'spectrum'}})
    glviewer.render()
    glviewer.zoomTo()                                                                  

def check_clicked_atoms(ev):
    global glviewer
    clicked_atoms=[]
    model = glviewer.getModel()
    atoms=model.selectedAtoms()
    for i in atoms:
        if 'clicked' in i.to_dict() and i.clicked ==True:
            clicked_atoms.append(int(i.serial))
    print(clicked_atoms)
    clicked_atoms=json.dumps(clicked_atoms)
    return clicked_atoms

################# SERVER CONNECTION FUNCTIONS #########################

def handle_file_dragged_over(evt):
    evt.stopPropagation()
    evt.preventDefault()
    evt.dataTransfer.dropEffect = 'copy'

def on_complete(evt) :
    global ajax_data
    #print("AJAX COMPLETED, returned:",evt.text)
    ajax_data = json.loads(evt.text)
    print(ajax_data)

def to_timeout():
    global URL
    get_score_table(0)
    update_progress(URL) #aio.run(update_progress)


def print_progress(request):
    print(request.text)
    data=json.loads(request.text)
    print(data)
    if data['state'] != 'PENDING' and data['state'] != 'PROGRESS': 
        if 'result' in data: 
            #show result
            document["download"].innerHTML="DONE!"
        
        else :
            # something unexpected happened
            document['download'].text='Error: ' + str(data['state'])
    else :
        # rerun in 2 seconds
        timer.set_timeout(to_timeout, 5000)


def update_progress(status_url): 
    global URL
    URL=status_url
    #req = await aio.ajax()
    req = ajax.ajax()
    req.bind('complete', print_progress)
    req.open('GET',status_url,True)
    req.send()


def listen(request):
    status_url = json.loads(request.text)['Location']
    print(status_url)
    update_progress(status_url)
    

def dock(ev) :
    global glviewer,ligand_code
    if document["download"].text=="New project":
        # for child in document["cmap"].children:
        #     child.style.display="inline"
        document["cmap"].style.display="block"
        document["map_pic"].style.display=None
        document["table"].style.display =None
        clear_vars()
        document["download"].text="Submit"
    else:

        document["download"].innerHTML="Docking..."
        username = document["username"].value
        # for child in document["cmap"].children:
        #     child.style.display=None
        document["cmap"].style.display=None
        req = ajax.ajax()
        req.open('POST', '/longtask', True)
        content = "application/x-www-form-urlencoded;charset=UTF-8"
        #content = "application/json;charset=UTF-8"
        req.set_header('content-type', content)
        req.bind('complete', listen)
        clicked_atoms=check_clicked_atoms(ev)
        try:
            params=json.dumps(files_for_docking['params'])
            ligand_code=params[0][:3]
            req.send({'username':document['username'].value,'project_name':document['project_name'].value,'pdb_name':files_for_docking['pdb_name'], 'params':params ,'jobs' : document['jobs'].value,'partners':document['partners'].value,'atoms':clicked_atoms,'first_cycle':document['first'].value,'second_cycle':document['second'].value})
        except:
            alert("Missing file or parameter!")

def send_ajax(data) :

    req = ajax.ajax()
    ending = name.split('.')[-1]
    if ending=='pdb':
        req.open('POST', "/process_pdb", True)
        files_for_docking['pdb_name']=name
        document["11"].text=name +' file loaded!'

        
    elif ending=="params":
        req.open('POST', '/save_params', True)
        files_for_docking['params'].append(name)
        if document["33"].text=="3. Optionaly you can drop your PARAMS files as well":
            document["33"].text=name +' file loaded!'
        else:
            first_name=document["3"].text.split()[0]
            document["33"].text=first_name+" and "+name +' files loaded!'
        
    elif ending=="sdf":
        req.open('POST', '/handle_sdf', True)
        files_for_docking['params'].append(name.split('.')[0]+".params")
        if document["22"].text=="2. Drop ligand file in one of this formats: SDF, MOL, MOL2":
            document["22"].text=name +' file loaded!'
        else:
            first_name=document["2"].text.split()[0]
            document["22"].text=first_name+" and "+name +' files loaded!'
        

    content = "application/x-www-form-urlencoded;charset=UTF-8"
    #content = "application/json;charset=UTF-8"
    req.set_header('content-type', content)
    req.bind('complete', on_complete)
    req.send({'data' : data,'filename':name,'username':document['username'].value,'project_name':document['project_name'].value})
    
def load_file(evt) :

    text = reader.result
    print(text[0:200])
    send_ajax(text)
    if name.split(".")[-1]=='pdb':
        document["text"].innerHTML=text
        event = window.MouseEvent.new("click")
        document["show_native"].dispatchEvent(event)

def handle_file_dropped(evt):
    global name

    evt.stopPropagation()
    evt.preventDefault()

    files = evt.dataTransfer.files  # FileList object.
    global reader
    reader = window.FileReader.new()
    name=files[0].name
    reader.onload = load_file
    ending = files[0].name.split('.')[-1]
    if ending!='pdb' and ending!="params" and ending!="sdf":
        alert("Unknown input format .%s"%(ending))

    reader.readAsText(files[0])

def return_labels(ev):
  id = ev.target.id
  x=float(document[id]["x"]) +float(document[id].width)/2
  y=float(document[id]["y"])+float(document[id].height)/2
  x_texts=document["XaxisTicsLabB"].get(selector='text')
  y_texts=document["YaxisTicsLabL"].get(selector='text')
  for i in range(len(x_texts)):
    if x_texts[i]["x"]==str(x):
        x=x_texts[i]
  for i in range(len(y_texts)):
    if y_texts[i]["y"]==str(y):
        return x,y_texts[i]

def bold_label(ev):
  
  ev.target["fill"]="red"
  x,y=return_labels(ev)
  x["fill"]="red"
  x["font-weight"]="bold"
  y["fill"]="red"
  y["font-weight"]="bold"


def normal_label(ev):
  ev.target["fill"]="SteelBlue"
  x,y=return_labels(ev)
  x["fill"]="black"
  x["font-weight"]="normal"
  y["fill"]="black"
  y["font-weight"]="normal"
  #cokolwiek

def draw_cmap(ligand_data,receptor_for_plot,ligand_fracs,ligand_labels,receptor_fracs,receptor_labels):
    document["cmap"].style.display=None
    document["map_pic"].style.display="block"

    drawing = HtmlViewport(document["map_pic"], 0, 0, 450, 450)


    pl = Plot(drawing,20,380,30,370,-1,len(receptor_labels)-2,min(ligand_data)-1,max(ligand_data)+1, axes_definition="UBLR")

    for ax_key in pl.axes:
        pl.axes[ax_key].are_tics_inside=False

    pl.axes["L"].tics_at_fraction(ligand_fracs,ligand_labels)
    pl.axes["U"].tics_at_fraction(receptor_fracs,receptor_labels)
    pl.axes["B"].tics_at_fraction(receptor_fracs,receptor_labels)
    pl.axes["R"].tics_at_fraction(ligand_fracs,ligand_labels)

    # pl.axes["U"].label="RECEPTOR"
    # pl.axes["L"].label="LIGAND"

    mrk_s=int(350/len(ligand_labels))
    
    pl.scatter(receptor_for_plot,ligand_data,markersize=mrk_s,markerstyle="s", colors = "SteelBlue")
    for loc,ax in pl.axes.items() :
      ax.width, ax.stroke = 2, color_by_name("SteelBlue").create_darker(0.3)
    pl.draw_axes()
    drawing.finalize()

    for i in document.select("rect"):
        i.bind("mouseover",bold_label)
        i.bind("mouseout",normal_label)
        i.bind("click",show_contact)


def prepare_data_cmap(to_display):
    global contacts,cntcs
    print("CONTANTS")
    print(contacts)
    
    if len(to_display)!=0:
        cntcs=contacts[to_display[0]]
    else:
        cntcs=contacts[0]

    #print(contacts)
    ligand_data=[]
    ligand_labels=[]
    receptor_data=[]
    cnt=0
    for ligand_atom in cntcs:
        for res in cntcs[ligand_atom]:
            ligand_data.append(cnt)
            receptor_data.append(int(res))
        ligand_labels.append(ligand_atom)
        cnt+=1

    sorted_receptor=sorted(receptor_data) #sort list
    sorted_receptor = list(dict.fromkeys(sorted_receptor)) #uniq list
    result = map(lambda x:str(x),sorted_receptor) # make a string from numbers
    receptor_labels = [" "]+list(result) 
    receptor_fracs = []

    for i in range(len(receptor_labels)):
        receptor_fracs.append(i/(len(receptor_labels)))
    receptor_fracs.append((i+1)/len(receptor_labels))
    receptor_labels.append(" ")

    receptor_for_plot=[]
    cnt=0
    for i in range(len(receptor_data)):
        receptor_for_plot.append(sorted_receptor.index(receptor_data[i]))

    ligand_labels = [" "]+ligand_labels
    ligand_fracs = []
    
    for i in range(len(ligand_labels)):
        ligand_fracs.append(i/(len(ligand_labels)))
    ligand_fracs.append((i+1)/len(ligand_labels))
    ligand_labels.append(" ")


    if len(cntcs)!=0:
        draw_cmap(ligand_data,receptor_for_plot,ligand_fracs,ligand_labels,receptor_fracs,receptor_labels)
    else:
        alert("There is no contacts for these decoy")

def add_cmap_data(ev):
    global contacts,hydrogens
    data = json.loads(ev.text)
    for name in data:
        contacts[name] = data[name]['contacts']
        hydrogens[name] = data[name]['hydrogens']

def get_cmap():
    global username,project_name
    req = ajax.ajax()
    req.open('POST', '/contact_map', True)
    content = "application/x-www-form-urlencoded;charset=UTF-8"
    req.set_header('content-type', content)
    req.bind('complete', add_cmap_data)
    #params=files_for_docking['params']
    if "username" in document and 'project_name' in document:
        username = document["username"].value
        project_name = document["project_name"].value
    #req.send({'pdb_name':name, 'ligand_code':params[0].split(".")[0] ,'cutoff' : document['cutoff'].value})
    req.send({'username' : username,'project_name' : project_name,'cutoff' : document['cutoff'].value})

def show_cmap(ev):
    
    document["table"].style.display="none"
    to_display=[]
    options = document.select('td')
    for i in options:
        if "class" in i.attrs and i["class"]=="bg-white":
            to_display.append(i.text)
    prepare_data_cmap(to_display)



def atomcallback(atom,viewer,b, a):
    res = viewer.selectedAtoms({'resi':atom.resi})
    for at in res:
        if 'clickLabel' not in at.to_dict() or at.clickLabel==None: 
        
            glviewer.addStyle({'resi':atom.resi}, {'stick': {'color':'spectrum'}})
            glviewer.render(); 
            
            for a in res:
                a.clicked=True
                if a.atom=="CA":
                    a.clickLabel = viewer.addLabel(str(atom.resn) + str(atom.rescode),
                                              {'fontSize' : 14, 
                                               'position' : {'x':a.x, 'y':a.y, 'z':a.z}})  
            glviewer.render();   
        else:
            for a in res:
                if a.atom=="CA":
                    viewer.removeLabel(a.clickLabel)
                    a.clickLabel = None;
                a.clicked=False

def add_hydrogen():
    global name
    atom_name='H2'
    coordinates = hydrogens[decoys_to_show[0]]
    m = glviewer.addModel()

    #m.addAtoms([{'elem': 'C', 'x': 0, 'y': 0, 'z': 0, 'bonds': [1,2], 'bondOrder': [1,2]}, {'elem': 'O', 'x': -1.5, 'y': 0, 'z': 0, 'bonds': [0]},{'elem': 'O', 'x': 1.5, 'y': 0, 'z': 0, 'bonds': [0], 'bondOrder': [2]}])
    for a in coordinates:
        glviewer.getModel(0).addAtoms([{'hetflag':True,'elem':a,'x':coordinates[a][0],'y':coordinates[a][1],'z':coordinates[a][2]}])
        at = glviewer.selectedAtoms({'hetflag':True,'elem':a})
        glviewer.setStyle({'hetflag':True,'elem':a},{'stick':{'color':'black'}})

        print(at[0].to_dict())
    #glviewer.zoomTo(a[0]);
    glviewer.render();
            

######################### Setup the listeners ############################
drop_rec = document['cmap']
drop_rec.bind('dragover', handle_file_dragged_over)
drop_rec.bind('drop', handle_file_dropped)
document["list_files"].bind('click',get_score_table)
document["show_native"].bind('click',show_prot)
document["download"].bind('click',dock)
document["cmap_button"].bind('click',show_cmap)

for i in document.select(".option"): i.bind('click',toggleTab)

print("Done!")
