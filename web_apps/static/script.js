const buttons = document.querySelectorAll('#sort');
var tables = document.querySelectorAll("#mytable");

function sortTable() {
    tables.forEach(table => {
        var tableData = table.getElementsByTagName('tbody').item(0);
        var rowData = tableData.getElementsByTagName('tr');
        for (var i = 0; i < rowData.length - 1; i++) {
            for (var j = 0; j < rowData.length - (i + 1); j++) {
                if (parseInt(rowData.item(j).getElementsByTagName('td').item(1).innerHTML) >
                    parseInt(rowData.item(
                        j + 1).getElementsByTagName('td').item(1).innerHTML)) {
                    tableData.insertBefore(rowData.item(j + 1), rowData.item(j));
                }
            }
        }
    })
}

buttons.forEach(button => button.addEventListener('click', sortTable));

// toggleTabs

const app = {
    pages: [],
    show: new Event('show'),
    init: function () {
        app.pages = document.querySelectorAll('.tabcontentleft');
        app.pages.forEach((pg) => {
            pg.addEventListener('show', app.pageShown);
        })
        document.querySelectorAll('.optionleft').forEach((link) => {
            link.addEventListener('click', app.nav);
        })
        history.replaceState({}, 'Home', '#home');
        window.addEventListener('popstate', app.poppin);
    },
    nav: function (ev) {
        ev.preventDefault();
        let currentPage = ev.target.getAttribute('data-target');
        document.querySelector('.activebutton').classList.remove('activebutton');
        ev.currentTarget.className += " activebutton";
        document.querySelector('.active').classList.remove('active');
        document.getElementById(currentPage).classList.add('active');
        history.pushState({}, currentPage, `#${currentPage}`);
        document.getElementById(currentPage).dispatchEvent(app.show);
    },

    poppin: function (ev) {
        console.log(location.hash, 'popstate event');
        let hash = location.hash.replace('#', '');
        document.querySelector('.active').classList.remove('active');
        document.getElementById(hash).classList.add('active');
        document.getElementById(hash).dispatchEvent(app.show);
    }
}

const app2 = {
    pages: [],
    show: new Event('show'),
    init: function () {
        app2.pages = document.querySelectorAll('.tabcontenright');
        app2.pages.forEach((pg) => {
            pg.addEventListener('show', app2.pageShown);
        })
        document.querySelectorAll('.optionright').forEach((link) => {
            link.addEventListener('click', app2.nav);
        })
        history.replaceState({}, 'Home', '#home');
        window.addEventListener('popstate', app2.poppin);
    },
    nav: function (ev) {
        ev.preventDefault();
        let currentPage = ev.target.getAttribute('data-target');
        document.querySelector('.activebutton2').classList.remove('activebutton2');
        ev.currentTarget.className += " activebutton2";
        document.querySelector('.active2').classList.remove('active2');
        document.getElementById(currentPage).classList.add('active2');
        history.pushState({}, currentPage, `#${currentPage}`);
        document.getElementById(currentPage).dispatchEvent(app2.show);
    },

    poppin: function (ev) {
        console.log(location.hash, 'popstate event');
        let hash = location.hash.replace('#', '');
        document.querySelector('.active2').classList.remove('active2');
        document.getElementById(hash).classList.add('active2');
        document.getElementById(hash).dispatchEvent(app2.show);
    }
}

document.addEventListener('DOMContentLoaded', app.init);
document.addEventListener('DOMContentLoaded', app2.init);